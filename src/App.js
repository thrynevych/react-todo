import React, { Component } from 'react';
import Todos from './Todos';
import AddTodo from './AddTodoForm';

class App extends Component {
  state = {
    todos: [
      {id: 1, content: "clean up the house"},
      {id: 2, content: "learn react"}
    ]
  }

  deleteTodo = (id) => {
    const todos = this.state.todos.filter(todo => {
      return todo.id !== id;
    })
    this.setState({
      todos
    })
  }

  addTodo = (todo) => {
    todo.id = Math.random();
    this.setState({
      todos: [...this.state.todos, todo]
    })
  }

  render() {
    return (
      <div className="app-todo container">
        <h1 className="center blue-text">Todo list</h1>
        <Todos todos={this.state.todos} deleteTodo={this.deleteTodo} />
        <AddTodo addTodo={this.addTodo} />
      </div>
    );
  }
}

export default App;
